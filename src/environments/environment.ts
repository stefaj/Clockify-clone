// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  firebase: {
    projectId: 'clockify-copy',
    appId: '1:377370875449:web:5362bf1e9f31f84da85fe8',
    storageBucket: 'clockify-copy.appspot.com',
    locationId: 'europe-west',
    apiKey: 'AIzaSyAiX1bvOYY4Sr6oEIWRLbB8oW0ggNri9PU',
    authDomain: 'clockify-copy.firebaseapp.com',
    messagingSenderId: '377370875449',
    measurementId: 'G-GF1LD7BX7J',
  },
  production: false
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
