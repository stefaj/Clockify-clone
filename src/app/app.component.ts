import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { Timestamp } from 'firebase/firestore';
import { PrimeNGConfig } from 'primeng/api';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  activities$: Observable<any>
  started: boolean = false
  form: FormGroup
  collectionName = "test"
  document: Object
  stopwatch: string = '00:00:00'
  startTimer
  hrs: number = 0
  mins: number = 0
  secs: number = 0
  stateOptions: any[]
  value1: string = 'timer'

  constructor(private afs: AngularFirestore,private fb: FormBuilder,private primengConfig: PrimeNGConfig){
    this.activities$ = afs.collection(this.collectionName, ref => ref.orderBy('start','desc')).valueChanges({idField: 'id'})
    this.stateOptions = [
      { label: 'Timer', value: 'timer' },
      { label: 'Manual', value: 'manual' },
    ];
  }

  ngOnInit(): void {
    this.primengConfig.ripple=true
    this.activities$.subscribe()
    this.initform()
  }

  initform(){
    this.form=this.fb.group({
      description: [],
      tags: this.fb.array([]),
      start: new Date,
      end: new Date,
    })
  }

  get description(){
    if(this.form.get("description").value == null)
      return ""
    return this.form.get("description").value
  }

  get tags(){
    return this.form.get("tags") as FormArray
  }

  get start(){
    return this.form.get("start").value as Date
  }

  get end(){
    return this.form.get("end").value as Date
  }

  onSubmit(){
    if(this.started){
      this.stopStopwatch()
      this.document["end"]=Timestamp.now().toMillis()
      this.afs.collection(this.collectionName).add(this.document)
    }else{
      if(this.value1=='manual'){
        this.document = {
          description: this.description,
          tags: this.tags.value,
          start: this.start.getTime(),
          end: this.end.getTime(),
        }
        if(this.document["start"]>this.document["end"]){
          alert("The start date must be before the end date")
          return
        }
        this.afs.collection(this.collectionName).add(this.document)
        return
      }else{
        this.startStopwatch()
        this.document = {
          description: this.description,
          tags: this.tags.value,
          start: Timestamp.now().toMillis(),
          end: 0,
        }
      }
    }
    this.started=!this.started
  }

  addTag(){
    this.tags.push(this.fb.control(''))
  }

  deleteTag(i:number){
    this.tags.removeAt(i)
  }

  startStopwatch(){
    this.startTimer = setInterval(()=>{
      this.secs++;
      if(this.secs==60){
        this.secs=0
        this.mins++
        if(this.mins==60){
          this.mins=0
          this.hrs++
        }
      }

      if(this.hrs<10)
        this.stopwatch='0'+this.hrs+':'
      else
        this.stopwatch=this.hrs+':'
      
      if(this.mins<10)
        this.stopwatch=this.stopwatch+'0'+this.mins+':'
      else
        this.stopwatch=this.stopwatch+this.mins+':'
      
      if(this.secs<10)
        this.stopwatch=this.stopwatch+'0'+this.secs
      else
        this.stopwatch=this.stopwatch+this.secs
    },1000)
  }

  stopStopwatch(){
    clearInterval(this.startTimer)
    this.secs=0
    this.mins=0
    this.hrs=0
    this.stopwatch = '00:00:00'
  }
}
