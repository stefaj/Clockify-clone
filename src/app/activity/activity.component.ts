import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import {MenuItem} from 'primeng/api';

@Component({
  selector: 'app-activity',
  templateUrl: './activity.component.html',
  styleUrls: ['./activity.component.css']
})
export class ActivityComponent implements OnInit {

  @Input() activity
  @Input() collectionName
  items: MenuItem[]
  description: string
  dateStart: Date
  dateEnd: Date
  time: string = "00:00:00"
  editing: boolean = false

  constructor(private afs: AngularFirestore) { 
    
  }

  ngOnInit(): void {
    this.items=[
      {
        label: 'Edit',
        icon: 'pi pi-refresh'
    },
    {
        label: 'Delete',
        icon: 'pi pi-times'
    },
    ]

    this.description=this.activity.description.trim() == 0 ? "No description" : this.activity.description
    this.dateStart=new Date(this.activity.start)
    this.dateEnd=new Date(this.activity.end)
    this.setTime()
  }

  setTime(){
    console.log(`end ${this.activity.end} start ${this.activity.start}`)
    this.msToTime(Math.abs(this.activity.end-this.activity.start));
  }

  msToTime(s) {
    var ms = s % 1000;
    s = (s - ms) / 1000;
    var secs = s % 60;
    s = (s - secs) / 60;
    var mins = s % 60;
    var hrs = (s - mins) / 60;
    var days=0;
    
    this.time=""

    if(hrs>23){
      days=Math.floor(hrs/24)
      hrs=hrs%24
      this.time=days + "d "
    }

    if(hrs < 10)
      this.time+= "0" + hrs + ":"
    else
      this.time+= hrs + ":"

    if(mins < 10)
      this.time+= "0" + mins + ":"
    else
      this.time+= mins + ":"

    if(secs < 10)
      this.time+= "0" + secs
    else
      this.time+= secs
  }

  delete(){
    this.afs.collection(this.collectionName).doc(this.activity.id).delete()
  }

  edit(){
    if(!this.editing){
      this.editing=!this.editing
      return
    }
    var document={
      description: this.description,
      start: this.dateStart.getTime(),
      end: this.dateEnd.getTime()
    }

    if(document.start>document.end){
      alert("The start date must be before the end date")
      return
    }

    this.afs.collection(this.collectionName).doc(this.activity.id).update(document)
    this.editing=!this.editing
  }

}
